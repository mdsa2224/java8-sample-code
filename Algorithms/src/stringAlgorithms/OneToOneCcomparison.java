package stringAlgorithms;

import java.util.Scanner;
import java.lang.Math;

public class OneToOneCcomparison {

	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the first String");		
		String s1 = sc.nextLine();
		
		System.out.println("Enter the second String");		
		String s2 = sc.nextLine();
		
		char ch1[] = s1.toCharArray();
		
		char ch2[] = s2.toCharArray();
		
		int count=0;
		
		for(int i=0 ; i<(s1.length()<s2.length()?s1.length():s2.length());i++)
		{
			
			if(ch1[i] != ch2[i])
			{
				count++;
			}
			
		}
		//System.out.println(count);
		
		if(s1.length()==s2.length())
		{
			System.out.println("number of mismatch " + count);
		}
		else 
		{
			int diff = Math.abs((s1.length()-s2.length()));
			System.out.println("number of mismatch "+(count+diff));
		}
	}
}
