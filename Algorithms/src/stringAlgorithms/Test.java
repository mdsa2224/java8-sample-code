package stringAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Test {

		
		public String reverse(String s)
		{
			if(s.length()==1)
			{
				System.out.println(s+" inside if");
				return s;
			}
			else
			{
				System.out.println(s.charAt(s.length()-1)+ " first line");
				System.out.println(s.substring(0, s.length()-1)+" 2nd line");
				return s.charAt(s.length()-1)+reverse(s.substring(0, s.length()-1));
			}
		}
		public int secondLargest(int[] arr)
		{
			int MAX=0;
			int MAX2=0;
			
			for(int i= 0 ;i<arr.length;i++)
			{
				if(arr[i]>MAX)
				{
					MAX2=MAX;
					MAX=arr[i];
				}
		
			}
			System.out.println(MAX2);
			
			return 0;
		}
		public String revString(String str)
		{
			
			char ch[]= str.toCharArray();
			char temp;
			
			for(int i= 0; i<ch.length/2;i++)
			{
				temp=ch[i];
				ch[i] = ch[ch.length-i-1];
				ch[ch.length-i-1]=temp;
			}
			return new String(ch);
		}
		public static String removeSpace(String str)
		{
			
			char ch[] = str.toCharArray();
			char chnew[] = new char[ch.length];
			int count=0;
			for(int i= 0; i<ch.length-1;i++)
			{
				if(ch[i]!=' ' || ch[i+1]!=' ')
				{
					chnew[count]=ch[i];
					count++;
				}
				
			}
			chnew[count]=ch[ch.length-1];
			
			return new String(chnew);
		}
		public static char duplicateCharString(String str)
		{
			Set<Character> set = new HashSet<Character>();
			char ch[] = str.toCharArray();
			char duplicate[] = new char[10];
			for(int i =0,j=0;i<ch.length;i++)
			{
				if(!set.add(ch[i]))
				{
					//return ch[i];
					duplicate[j]=ch[i];
					j++;
				}
			}
			System.out.println(new String(duplicate));
			return ' ';
		}
			public static void main(String[] args) {
				//System.out.println(new Test().reverse("Salim"));
				int arr[]= {7,6,4,9};
				/*new Test().secondLargest(arr);*/
				//System.out.println(new Test().revString("AMAZE"));
				//System.out.println("what  is     this".replaceAll("\\s+", " "));
				/*System.out.println(removeSpace("what    is    this    "));*/
				
				/*Map<Integer, Integer> map = new HashMap<Integer, Integer>();
				map.put(1,1);
				map.put(2,4);
				map.put(5,25);
				map.put(3,9);
				map.put(4,16);
				map.put(16,4);
				
				System.out.println(map);
				System.out.println(sortMap(map));*/
				//System.out.println(duplicateCharString("manman"));
				//System.out.println(recursiveSum(1235));
				//System.out.println(isPrime(27));
				/*System.out.println(isArmstrong(371));
				List<Integer> list = new ArrayList<Integer>();
				
				list.add(10);
				list.add(9);
				list.add(5);
				list.add(78);
				
				System.out.println(sortList(list));*/
				
			
				
			//	countNumberOfElementInList();
				duplicateTest();
				
			}	
			
		public static List<Integer> sortList(List<Integer> list){
			
			Collections.sort(list,new Comparator<Integer>(){
				@Override
				public int compare(Integer o1,Integer o2) {
					// TODO Auto-generated method stub
					return o2.compareTo(o1);
				}
			});
		
			return list;
		}
		
		
		public static List<Map.Entry<Integer, Integer>> sortMap(Map<Integer, Integer> map)
		{
			List<Map.Entry<Integer, Integer>> list = new  ArrayList<Map.Entry<Integer, Integer>>(map.entrySet());
			
			Collections.sort(list,new Comparator<Map.Entry<Integer, Integer>>(){
				
				public int compare(Map.Entry<Integer, Integer> o1,Map.Entry<Integer, Integer> o2)
				{
					return o1.getValue().compareTo(o2.getValue());
				}
			});
			return list;
		}
		public static boolean isPrime(int num)
		{
			boolean isPrime = true;
			for(int i = 2;i<num/2;i++)
			{
				if(num%i==0)
				{
					isPrime=false;
					break;
				}
			}
			
			
			return isPrime;
		}
		
		public static int recursiveSum(int num)
		{
			if(num/10==0)
			{
				return num;
			}
			else
			{
				return num%10+recursiveSum(num/10);
			}
			
			//return 0;
		}
		public static boolean isArmstrong(int num)
		{
			boolean isArmstrong= false;
			
			int sum1 = 0 ;
			int temp = num;
			while(num!=0)
			{
				sum1+=Math.pow(num%10,3);
				num=num/10;
			}
			if(temp == sum1)
			{
				isArmstrong = true;
			}
			
			return isArmstrong;		
			}
		public static void countNumberOfElementInList() {
			
			List<String> list = new ArrayList<String>();
			
			list.add("one");
			list.add("two");
			list.add("three");
			list.add("one");
			list.add("one");
			list.add("three");
			list.add("four");
			
			
			Map<String,Integer> map = new HashMap<String, Integer>();
			
			Integer count = 0;
			for(String str : list) {
				count = 0;
			
				if((count = map.put(str,1))!=null) {
					map.put(str,++count);
				}
				
				/*if(map.get(str) ==null) {
				map.put(str, 1);
				} else {
					count = map.get(str);
					map.put(str, ++count);
				}*/
			}
			System.out.println(map);
			
			TreeMap<Integer,String> sortedMap = new TreeMap<Integer, String>();
			for(Map.Entry<String, Integer> entry : map.entrySet()) {
				
				if(sortedMap.get(entry.getValue()) ==null ){
					sortedMap.put(entry.getValue(), entry.getKey());
				}
				else {
					
				}
			}
			
			System.out.println(sortedMap);
			

			System.out.println(sortedMap.lastEntry());
			
			Map map1 = new HashMap();
			
		//	map1.put("One", 1);
			
			System.out.println(map1.put("One", 5));
			
			System.out.println(map1.get("One"));
			
			return;
		}
		
		public static void duplicateTest() {
			
			int arr[] = {4, 3, 6, 2, 1, 1};
			
			Arrays.sort(arr);
			
			System.out.println(arr);
			
			for(int i=0;i<arr.length;i++) {
				if(i+1!=arr[i]) {System.out.println("Missing " +(i+1));continue;}
				
				if(i<arr.length -1 && arr[i]==arr[i+1]) {System.out.println("repeated "+arr[i]);continue;}
			}
			
			
		}
		
}
