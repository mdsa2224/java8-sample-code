package stringAlgorithms;

import java.util.Scanner;

public class PatternBased {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the first string");
		String str1 = (String)sc.nextLine();
		System.out.println("entr the 2nd string");
		String str2 = (String)sc.nextLine();
		
		char ch1[] = str1.toCharArray();
		char ch2[] = str2.toCharArray();
		
		int arr[] = new int[str2.length()*2];
		
		int lastOccur = -1;
		int firstOccur=-1;
		int k=0;
		for(int i=0;i<ch2.length;i++)
		{
			
			boolean firstCheck = true;
			for(int j=0;j<ch1.length;j++)
			{
				
				if(ch2[i]==ch1[j])
				{
					lastOccur=j;
					if(firstCheck)
					{
						firstOccur=j;
						firstCheck=false;
					}
				}
				
			}
			arr[k]=firstOccur;
			arr[k+1]=lastOccur;
			k+=2;
		}
		boolean flag = true;
		for(int i=0;i<arr.length-1;i++)
		{
			if(arr[i]>arr[i+1])
			{
				flag = false;
				break;
			}
		}
		System.out.println(flag);
		sc.close();
	}
}
