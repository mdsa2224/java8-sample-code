package searchingAlgorithm;

public class BinarySearch {

	
	public static void main(String[] args) {
		
		
		int arr[]={1,5,7,8,9,15,48};
		int mid=0;
		int lower=0;
		int upper=arr.length-1;
		int search=1;
		boolean flag=false;
		while(lower<=upper)
		{
			mid=(lower+upper)/2;
			
			if(arr[mid]==search)
			{
				System.out.println("the position is "+mid);
				flag=true;
				break;
			}
			else if(arr[mid]>search)
			{
				upper=mid-1;
			}
			else 
			{
				lower=mid+1;
				
			}
			
		}
		if(!flag)
		{
			System.out.println("element not found");
		}
		
	}
}
