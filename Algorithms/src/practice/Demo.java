package practice;

import java.util.Arrays;
import java.util.Scanner;

public class Demo {

	public static void main(String[] args) {
		//System.out.println(findLargestFactor());
		removeSpace();
		//System.out.println(36%48);
	}
	
	public static int findLargestFactor()
	{
		int n1 = 50;
		int n2 = 20;
		
		int small = n1<n2?n1:n2;
		int large = n1>n2?n1:n2;
		
		if(large%small==0)return small;
		
		for(int i=small/2;i>0;i--)
		{
			if(large%i==0 && small%i==0)return i;
		}
		return 1;
		
	}
	public static void secondMax() {
		
		int arr[]  ={10,9,2,3,15,7};
		
		int max=0;
		int secondMax =0;
		
		for(int i=0 ;i <arr.length;i++) {
			
			if(arr[i]>max) {
				secondMax=max;
				max = arr[i];
			} else if(arr[i]>secondMax){
				secondMax=arr[i];
			}
			
		}
		System.out.println(secondMax);
	}
	public static void removeSpace() {
		
		String s = "this    word    has extra  space   ";
		
		char ch[] = s.toCharArray();
		
		char result[] = new char[s.length()];
		
		for(int i = 0,j=0;i<ch.length-1;i++) {
			
			if(!(ch[i]==' ' && ch[i+1] == ' ')) {
				result[j] = ch[i];
				j++;
			}
		}
		
		
		String s1= new String(result);
		System.out.println(s1);
	}
	
	
	
	public static void duplicateChar() {
		
		String s = "abhgb";
		
		char ch[] = s.toCharArray();
		
		Arrays.sort(ch);
		char dup[] = new char[s.length()];
		
		for(int i =0,j=0; i<ch.length-1;i++ ) {
			if(ch[i]==ch[i+1]) {
				dup[j] =ch[i]; j++;}
		}
		
		String s1 = new String(dup);
		System.out.println(s1);
		
	}
	
}
