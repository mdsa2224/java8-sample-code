package sortingAlgorithm;

public class SelectionSort {

	public void selectionSort(int []arr)
	{
		int temp=0;
		
		int minIndex=0;
	for(int j=0;j<=arr.length-2;j++)
	{
		minIndex=j;
		for(int i=j+1;i<=arr.length-1;i++)
		{
			if(arr[minIndex]>arr[i])
			{
				minIndex=i;
			}
			
		}
		if(minIndex!=j)
		{
		temp =arr[minIndex];
		arr[minIndex] = arr[j];
		arr[j] = temp;
		}
	}
	printArray(arr);
	}
	
	public static void main(String[] args) {
		
		int arr[] = {7,2,4,1,5,3};
		new SelectionSort().selectionSort(arr);
		System.out.println();
		
	}
	
	public void printArray(int[] arr)
	{
		for(int a:arr)
			System.out.print(a+", ");
	}
}
