package sortingAlgorithm;

public class InsertionSort {
	
	
	public void sort(int[] arr)
	{
		int key =0;
		
		for(int i=0;i<arr.length-1;i++)
		{
			key = arr[i+1];
			for(int j=i+1;j>=0;j--)
			{
				if(key<arr[j])
				{
				arr[j+1] = arr[j];
				arr[j]=key;
				}
			}
			
		}
		printArray(arr);
	}
	public void insertionSort1(int arr[])
	{
		int temp=0;
		for(int i=1;i<arr.length-1;i++)
		{
			temp=arr[i];
			for(int j=i-1;j>=0;j--)
			{
				if(arr[i]<arr[j])
				{
					arr[j+1]=arr[j];
					arr[j]=temp;
				}
			}
		}
		printArray(arr);
	}

	public static void main(String[] args) {
		
		int arr[] = {7,2,4,1,5,3};
		new InsertionSort().sort(arr);
		System.out.println();
		new InsertionSort().insertionSort1(arr);
		
	}
	
	public void printArray(int[] arr)
	{
		for(int a:arr)
			System.out.print(a+", ");
	}
	
}
