package sortingAlgorithm;

public class BubbleSort {

	public void bubbleSort(int[] arr)
	{
		int temp=0;
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length-1;j++)
			{
				if(arr[j]>arr[j+1])
				{
					temp = arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		printArray(arr);
	}

	public static void main(String[] args) {
		
		int arr[] = {7,2,4,1,5,3};
		new BubbleSort().bubbleSort(arr);
		System.out.println();
		
	}
	
	public void printArray(int[] arr)
	{
		for(int a:arr)
			System.out.print(a+", ");
	}
	public void swap(int []arr,int i,int j)
	{
		
	}
}
