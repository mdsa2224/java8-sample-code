package sortingAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SortMapByValue {

	public static void main(String[] args) {
		
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("two", 2);
		map.put("one", 1);
		
		map.put("five", 5);
		map.put("zero", 0);
		map.put("four", 4);
		System.out.println(map);
		//TreeMap<Integer, String> tMap = new TreeMap<Integer, String>();
		
	//	List<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(map.entrySet()); 
		
	
		/*Collections.sort(list,
				
		new Comparator<Map.Entry<Integer, String>>() {
			
			public int compare(Map.Entry<Integer, String> o1, Map.Entry<Integer, String> o2) {
				
				return o1.getValue().compareTo(o2.getValue());
			}
		
		});*/
		
		//lamda expression can also be used.
		/*Collections.sort(list,(o1,o2) -> {return o1.getValue().compareTo(o2.getValue());} );
		
		list.forEach(m ->System.out.println(m));*/
		
		
		//System.out.println(map);
		
		map = sortMap(map);
		
		System.out.println(map);
	}
	
	//you can build seperate function also to sort.
	
	public static <K, V extends Comparable<? super V>> Map<K,V> sortMap(Map<K,V> map) {
		
		
		List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
		
		Collections.sort(list, (o1,o2) -> {return o1.getValue().compareTo(o2.getValue());});
		
		Map<K,V> map1 = new LinkedHashMap<K,V>();
		
		for(Map.Entry<K, V> m : list) {
			System.out.println(m);
			map1.put(m.getKey(), m.getValue());
		}
		
		//System.gc();
		return map1;
		
	}
	
	public <T> List<T> sortSomething(T t) {
		
		return null;
	}
	
 }
