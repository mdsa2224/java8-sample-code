package sortingAlgorithm;

public class QuickSort {

	public static void quickSort(int[] arr,int low,int high)
	{
		int pIndex = partition(arr,low,high);
		quickSort(arr, low, pIndex-1);
		quickSort(arr,pIndex+1,high);
	}
	
	public static int partition(int[] arr,int low,int high)
	{
		int pivot = arr[high];
		
		int i = low;
		int j = high-1;
		
		while(i<j)
		{
			while(arr[i]<pivot && arr[i]!=pivot)
				i++;
			while(arr[j]>pivot && arr[i]!=pivot)
				j--;
			swap(arr,i,j);
			//i++;j--;
		}
		if(i<j)
		{
			swap(arr,i,high);
		}
		return j;
	}
	public static void swap(int arr[],int i,int j)
	{
		int temp;
		temp = arr[i];
		arr[i] = arr[j];
		arr[j] =temp;
	}
	
	public static void main(String[] args) {
		
		int arr[] ={12,10,9,5,34,53,81,13,50};
		quickSort(arr, 0, arr.length-1);
	}
}
