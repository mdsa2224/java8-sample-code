package comparatorExample;

public class StudentDto implements Comparable{
	
	private String name;
	private int marks;
	private int rollNo;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	@Override
	public String toString() {
		return "StudentDto [name=" + name + ", marks=" + marks + ", rollNo="
				+ rollNo + "]";
	}
	@Override
	public int compareTo(Object o) {
		
		return 0;
	}
	
	

}
