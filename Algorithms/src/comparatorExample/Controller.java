package comparatorExample;

import java.util.*;

public class Controller{

	
	public static void main(String[] args) {
		
		Set<StudentDto> set = new TreeSet<StudentDto>();
		
		StudentDto dto1 = new StudentDto();
		dto1.setMarks(14);
		dto1.setName("rahul");
		dto1.setRollNo(12);
		
		StudentDto dto2 = new StudentDto();
		dto2.setMarks(15);
		dto2.setName("rahul");
		dto2.setRollNo(11);
		
		set.add(dto1);
		set.add(dto2);
		
		Iterator it = set.iterator();
		
		while(it.hasNext())
		{
		System.out.println(it.next());
		}
		
	}

}
