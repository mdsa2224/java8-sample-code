package dataStructureImpl;

// the below implementation is of double linklist
class Node<T>
{
	private T data;
	private Node<T> prev;
	private Node<T> next;
	public Node(){}
	public Node(T data){this.data=data;}
	public Node(Node<T> prev,T data,Node<T> next){this.prev=prev;this.data=data;this.next=next;}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Node<T> getPrev() {
		return prev;
	}
	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}
	public Node<T> getNext() {
		return next;
	}
	public void setNext(Node<T> next) {
		this.next = next;
	}
	@Override
	public String toString() {
		return "Node [data=" + data + "]";
	}
}

public class LinkedListC<T> {

	private int size;
	
	Node<T> first;
	Node<T> last;
	private StringBuilder linkString;
	
	public int add(T data)
	{
		if(first==null)
		{
			first = new Node<T>(data);
			size++;
		}
		else
		{
			if(last==null){
			last = new Node<T>(data);
			last.setPrev(first);
			first.setNext(last);
			size++;
			}
			else
			{
				Node<T> l = last;
				last = new Node<T>(data);
				last.setPrev(l);
				l.setNext(last);
				size++; 
			}
			
		}
		return size;
	}
	
	

	private Node<T> searchNode(Node<T> node,T data)
	{
		if(node==null)
		{
			//System.out.println("empty list");
			return null;
		}
		if(node.getData().equals(data))
		{
			return node;
		}
		return searchNode(node.getNext(),data);
		
	}
	public Node<T> searchData(T data)
	{
		return searchNode(first,data);
	}
	private void printList(Node<T> node)
	{
		if(node==null)
		{
			return;
		}
		linkString.append(node.getData()+",");
		printList(node.getNext());
		
	}
	//not using below function
	public boolean deleteNode(T data)
	{
		Node<T> node = searchNode(first, data);
		if(null==node)
		{
			return false;
		}
		if(node.getPrev()==null)
		{
			//delete first
			return true;
		}
		if(node.getNext()==null)
		{
			//delete last
			return true;
		}
		
		return true;
	}
	
	private void deleteFirst()
	{
		if(null ==first)
		{
			return;
		}
		if(first.getNext()==null)
		{
			first =null;
			return;
		}
		Node<T> node = first;
		node.getNext().setPrev(null);
		first=node.getNext();
	}
	private void deleteLast()
	{
		if(first==null) {
			return;
		}
		if(last==null)
		{
			return;
		}
		if(first.getNext()==null)
		{
			deleteFirst();
			return;
		}
		Node<T> node = last;
		last =null;
		node.getPrev().setNext(null);
		last = node.getPrev();
			
	}
	public boolean delete(T data)
	{
		if(null==data)
		{
			return false;
		}
		if(data.equals(first.getData()))
		{
			deleteFirst();
			return true;
		}
		Node<T> node =searchData(data);
		if(null==node)
		{
			return false;
		}
		if(node.equals(last))
		{
			deleteLast();
			return true;
		}
		Node<T> prev = node.getPrev();
		Node<T> next = node.getNext();
		node=null;
		prev.setNext(next);
		next.setPrev(prev);
		
		return true;
	}
	public String toString()
	{
		if(first==null) {
		linkString = new StringBuilder("[]");
		} else {
			linkString = new StringBuilder("[");
			printList(first);
			linkString = linkString.replace(linkString.length()-1, linkString.length(), "");
			linkString.append("]");
		}
		return linkString.toString();
	}
	
	public static void main(String[] args) {
		
		LinkedListC<Integer> list = new LinkedListC<Integer>();
		
		list.add(4);
		
		list.add(5);
		
		list.add(20);
		list.add(20);
		list.add(20);
		
		list.add(30);
		list.add(30);
		list.add(30);
		list.add(30);
		
		System.out.println(list);
		
		list.delete(20);
		
		System.out.println(list);
		
		
	}
}
