package dataStructureImpl;

class Leaf
{
	private int data;
	private Leaf left;
	private Leaf right;
	public Leaf(){}
	public Leaf(int data){this.data=data;}
	public Leaf(Leaf left,int data,Leaf right){this.left=left;this.data=data;this.right=right;}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public Leaf getLeft() {
		return left;
	}
	public void setLeft(Leaf left) {
		this.left = left;
	}
	public Leaf getRight() {
		return right;
	}
	public void setRight(Leaf right) {
		this.right = right;
	}
	/*@Override
	public String toString() {
		return "Leaf [data=" + data + "]";
	}*/
}
public class BinaryTree {

	private Leaf rootLeaf;
	
	public boolean add(int data) {
		
		if(rootLeaf==null){
			
			rootLeaf = new Leaf();
			rootLeaf.setData(data);
			return true;
		}
		if(data<=rootLeaf.getData())
		{
			addLeft(rootLeaf, data);
			return true;
		}
		if(data>rootLeaf.getData())
		{
			addRight(rootLeaf, data);
			return true;
		}
			
			return false;
	}
	
	
	public void addLeft(Leaf leaf,int data)
	{
		if(leaf==null)
		{
			Leaf left = new Leaf();
			left.setData(data);
			leaf=left;
			//leaf.setLeft(left);
		}
		else if(data<=leaf.getData())
		{
			addLeft(leaf.getLeft(),data);
		}
		else
		{
			addRight(leaf.getRight(),data);
		}
	}
	public void addRight(Leaf leaf,int data){
		
		if(leaf==null)
		{
			Leaf right = new Leaf();
			right.setData(data);
			leaf=right;
			//leaf.setRight(right);
		}
		else if(data<=leaf.getData())
		{
			addLeft(leaf.getLeft(),data);
		}
		else
		{
			addRight(leaf.getRight(),data);
		}
	}
	
	private boolean insertNode(int data)
	{
		if(rootLeaf ==null)
		{
			rootLeaf = new Leaf(data);
			return true;
		}
		return insertNode(rootLeaf,data) !=null;
		
	}
	private Leaf insertNode(Leaf leaf,int data)
	{
		if(leaf==null)
		{
			leaf = new Leaf();
			leaf.setData(data);
			
		}
		else if(data<=leaf.getData())
		{
			//return  insertNode(leaf.getLeft(),data);
			Leaf left = insertNode(leaf.getLeft(),data);
			leaf.setLeft(left);
		}
		else {
			Leaf right = insertNode(leaf.getRight(),data);
			leaf.setRight(right);
		}
		return leaf;
	}
	public boolean searchNode(int data)
	{
		if(rootLeaf==null)
		{
			return false;
		}
		if(rootLeaf.getData()==data)
		{
			return true;
		}
		if(data<rootLeaf.getData()) {
			 if(searchNode(rootLeaf.getLeft(), data)!=null) return true;
			 return false;
		}
		else {
			 if(searchNode(rootLeaf.getRight(), data)!=null) return true;
			 return false;
		}
		
		//return null;
	}
	public Leaf searchNode(Leaf leaf,int data)
	{
		if(leaf==null)
		{
			return null;
		}
		if(leaf.getData()==data)
		{
			System.out.println(leaf);
			return leaf;
		}
		if(data>leaf.getData()) {
			leaf = searchNode(leaf.getRight(),data);
		}
		else {
			leaf =searchNode(leaf.getLeft(),data);
		}
		return leaf;
	}
	public static void main(String[] args) {
		
		BinaryTree tree = new BinaryTree();
		
		tree.insertNode(10);
		tree.insertNode(8);
		tree.insertNode(9);
		tree.insertNode(5);
		tree.insertNode(2);
		tree.insertNode(13);
		tree.insertNode(15);
		tree.insertNode(16);
	
		tree.insertNode(12);
		
		//System.out.println(tree.max());
		tree.deleteLeaf(2);
		System.out.println(tree);
	//	System.out.println(tree.searchNode(173));
	}
	public Leaf max()
	{
		return findMax(rootLeaf);
	}
	private Leaf findMax(Leaf leaf)
	{
		if(leaf==null) {
			return null;
		}
		if(leaf.getRight()==null)
			return leaf;
		return findMax(leaf.getRight());
	//	return null;
	}
	private int countNode(Leaf leaf)
	{
		int nodeCount=0;
		if(rootLeaf==null)
			nodeCount=-1;
		else if(leaf ==null)
			nodeCount=-1;
		else if(leaf.getLeft()==null && leaf.getRight() ==null)
			 nodeCount = 0;
		else if(leaf.getLeft()!=null && leaf.getRight() !=null)
			nodeCount=2;
		else nodeCount=1;
		return nodeCount;
	}
	public void deleteLeaf(int data)
	{
		
		Leaf leaf = searchNode(rootLeaf,data);
		int nodeCount = countNode(leaf);
		if(nodeCount==-1)
		{
			System.out.println("nothing to delete");
		}
		else if(nodeCount==0)
		{
			System.out.println(leaf);
			leaf=null;
			System.out.println(leaf);
		}
		else if(nodeCount==1)
		{
			if(leaf.getLeft()!=null)
			{
				leaf=leaf.getLeft();
				//leaf.setLeft(null);
			}
			if(leaf.getRight()!=null)
			{
				leaf=leaf.getRight();
				leaf.setRight(null);
			}
		}
		else if(nodeCount==2)
		{
			Leaf max = findMax(leaf);

			leaf.setData(max.getData());
			max=null;
			
		}
		
	}
}
