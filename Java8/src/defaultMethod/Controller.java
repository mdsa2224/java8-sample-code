package defaultMethod;

public class Controller implements Interface1,Interface2{

	@Override
	public void method2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void method1() {
		// TODO Auto-generated method stub
		
	}
	

	/**We can declare method body in interface in JAVA 8, by using default keyword as declared in 
	 * Interface1.
	 * what about the diamond problem, here we have 2 interfaces Interface 1 and Interface 2
	 * when our class Controller implements both the Interfaces diamond problem
	 * arises, which gives a compiler error
	 * "Duplicate default methods named doJob with the parameters () and () are
	 *  inherited from the types Interface2 and Interface1"
	 *  To Avoid this we must override doJob method. 
	 *  So In JAVA 8 there is no chance of DIAMOND problem.
	 */

	@Override
	public void doJob() {
		System.out.println("I am in controller");
	}
		
	public static void main(String[] args) {
		
		new Controller().doJob();
	}
}
