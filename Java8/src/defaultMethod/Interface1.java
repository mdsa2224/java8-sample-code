package defaultMethod;

public interface Interface1 {

	public void method1();
	
	default void doJob() {
		System.out.println("Declared method in interface 1");
	}
}
