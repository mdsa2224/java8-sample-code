package defaultMethod;

public interface Interface2 {

	public void method2();
	
	default void doJob() {
		System.out.println("Declared method in interface 2");
	}
}
