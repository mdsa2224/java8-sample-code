package lamdaExpression;

import java.lang.reflect.Method;


//https://www.youtube.com/watch?v=yb46iD5dJYY&index=5&list=PLsyeobzWxl7qbvNnJKjYbkTLn2w3eRy1Q


class Employee implements Salary {
	@Override
	public void calculateSalary() {

		System.out.println("Salary calculated for employee");
	}
	
	private void call() {
		System.out.println("I am called.");
	}
}


public class LamdaTest {
	
	public static void main(String[] args) throws Exception {

		Salary s1 = new Employee();
		
		s1.calculateSalary();
		
		Salary s2 = new Salary() {
			
			public void calculateSalary() {
				System.out.println("Salary calculated for employee");
				
			}
		};
		
		s2.calculateSalary();
		//whatever we are doing in above 2 cases, can be easily done in a single
		//line using lamda expression as shown below
		Salary s3 =  () -> System.out.println("salary calculated");
		
		s3.calculateSalary();
	
		
	}

}
